# Résolution de conflits de fusion avec création d'une PR dans Bitbucket

Repo test contenant une branche `master` contenant elle-même :

- une branche `preprod`
    * une branche `feature` (créée à partir de la branche `preprod`)

On cherche à fusionner la branche `feature` dans la branche `preprod`. Un conflit va apparaître dans le fichier `fichier.php`.

**Contenu de `fichier.php` dans la branche `preprod` :**
```
<?php

$variable_1 = 'je ne génère pas de conflit';

$variable_2 = 'je suis créé depuis la branche preprod';
```

**Contenu de `fichier.php` dans la branche `feature` :**
```
<?php

$variable_1 = 'je ne génère pas de conflit';

$variable_2 = 'je suis créé depuis la branche feature';
```

La déclaration de `$variable_2` va générer un conflit (pas celle de `variable_1`)